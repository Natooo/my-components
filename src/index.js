import { MySlider } from './js/MySlider';
import { MyTodo } from './js/MyTodo'

const images = [
    'https://1.bp.blogspot.com/-1zfr4bupgXA/V9h0JZRa_ZI/AAAAAAAAiT4/fxiqRadRi3EmKYM03qr1W7psIW1_dXnOQCLcB/s1600/Selection_7_Raclette.jpg',
    'https://www.greatdanepub.com/sites/greatdanepub.com/files/menu-items/brat-bacon-burger.png',
    'http://tony-pizza.fr/Files/Image/Photos/Accueil/Full/Pizza-f.jpg',
    'http://s1.1zoom.me/big0/900/242854-dorren.jpg',
    'https://i.pinimg.com/originals/3a/77/1f/3a771f0dcc3bdb3733782a6036ae1bf4.jpg'
]

const ms = new MySlider('#ms-slider', images);
const mtdl = new MyTodo('#mtdl');
