import { Component } from './Component.js';
import { MyLightBox } from './MyLightBox.js';
import '../styles/MySlider.css';

export class MySlider extends Component {
    constructor(selector, imageries) {
        super();
        let self = this;
        let slider = document.querySelector(selector);

        this.lb = new MyLightBox(imageries);
        this.container = this.createElement('div', {'class': 'ms'});
        this.box = this.createElement('div', {'class': 'ms-box'});
        this.panelLeft = this.createElement("div", {"class": "ms-pnl-l ms-pnl"});
        this.panelRight= this.createElement("div", {"class": "ms-pnl-r ms-pnl"});
        this.images = this.createElement('div', {'class': 'ms-images'});
        this.thumbs = this.createElement('div', {'class': 'ms-thumbs'});
        this.createBtnLeft = this.createElement('div', {'class': 'ms-btn ms-btn-left'});
        this.createBtnRight = this.createElement('div', {'class': 'ms-btn ms-btn-right'});
        this.left = this.createElement("span", {'class': "ms-left"});
        this.right = this.createElement("span", {'class': "ms-right"});

        this.createBtnRight.appendChild(this.right);
        this.createBtnLeft.appendChild(this.left);
        this.right.innerHTML = "&#10095";
        this.left.innerHTML = "&#10094";
        this.panelRight.appendChild(this.createBtnRight);
        this.panelLeft.appendChild(this.createBtnLeft)
        this.box.appendChild(this.images);
        this.box.appendChild(this.thumbs);
        this.box.appendChild(this.panelLeft);
        this.box.appendChild(this.panelRight);
        this.container.appendChild(this.box);
        slider.appendChild(this.container);

        for (let i = 0; i < imageries.length; i++) {
            let img = this.createElement('img', {'src': imageries[i]});
            let image = this.createElement('div', {'class': 'ms-image'});
            let dot = this.createElement('span', {'class': 'ms-dot'});

            image.appendChild(img);
            this.images.appendChild(image);
            this.thumbs.appendChild(dot);

            let cb = function(image, dot) {
                return function(e) {
                    self.container.querySelector(".ms-image-active").classList.remove('ms-image-active');
                    self.container.querySelector(".ms-dot-active").classList.remove('ms-dot-active');
                    dot.classList.add('ms-dot-active');
                    image.classList.add('ms-image-active');
                }
            }

            dot.addEventListener('click', cb(image, dot));

            image.addEventListener('click', (function(i) {
                return function(e) {
                    self.lb.show(i);
                }
             })(i));
        }

        this.images.childNodes[0].classList.add("ms-image-active");
        this.thumbs.childNodes[0].classList.add('ms-dot-active');

        this.panelRight.addEventListener('mouseenter', this.showPanels.bind(this), false);
        this.panelLeft.addEventListener('mouseenter', this.showPanels.bind(this), false);
        this.panelRight.addEventListener('mouseleave', this.hidePanels.bind(this), false);
        this.panelLeft.addEventListener('mouseleave', this.hidePanels.bind(this), false);
        this.createBtnLeft.addEventListener('click', this.inputLeftA.bind(this), false);
        this.createBtnRight.addEventListener('click', this.inputRightA.bind(this), false);
    }

    inputRightA(e) {
        let imgActiveR = this.container.querySelector(".ms-image-active");
        let dotActiveR = this.container.querySelector(".ms-dot-active");
        let next = imgActiveR.nextSibling ? imgActiveR.nextSibling : this.images.childNodes[0];
        let nextDot = dotActiveR.nextSibling ? dotActiveR.nextSibling  : this.thumbs.childNodes[0];

        imgActiveR.classList.remove("ms-image-active");
        next.classList.add("ms-image-active");
        dotActiveR.classList.remove("ms-dot-active");
        nextDot.classList.add("ms-dot-active");
    }

    inputLeftA(e) {
        let imgActiveL = this.container.querySelector(".ms-image-active");
        let dotActiveL = this.container.querySelector(".ms-dot-active");
        let prev = imgActiveL.previousSibling ? imgActiveL.previousSibling : this.images.childNodes[this.images.childNodes.length-1];
        let prevDot = dotActiveL.previousSibling ? dotActiveL.npreviousSibling : this.thumbs.childNodes[this.thumbs.childNodes.length-1];

        imgActiveL.classList.remove("ms-image-active");
        prev.classList.add("ms-image-active");
        dotActiveL.classList.remove("ms-dot-active");
        prevDot.classList.add("ms-dot-active");
    }

    showPanels(e) {
        this.panelLeft.classList.add("ms-pnl-active");
        this.panelRight.classList.add("ms-pnl-active");
    }

    hidePanels(e) {
        this.panelLeft.classList.remove("ms-pnl-active");
        this.panelRight.classList.remove("ms-pnl-active");
    }
}
