import { Component }from './Component';
import '../styles/MyTodo.css';

export class MyTodo extends Component {
  constructor(selector) {
    super();

    let self = this;
    let todo = document.querySelector(selector);

    if (todo === null) {
      throw "invalid selector: " + selector;
    }

    this.container = this.createElement('div', {'class': 'mtdl-container'});
    this.title = this.createElement('h1', {'class': 'mtdl-title'});
    this.titleTxt = document.createTextNode('My Todo List');
    this.inputs = this.createElement('div', {'class': 'mtdl-input-btns'});
    this.input = this.createElement('input', {'class': 'mtdl-input-txt', 'type': 'text', 'placeholder': 'Enter Item..'});
    this.createBtn = this.createElement('input', {'class': 'mtdl-add-btn', 'type': 'button', 'value': 'Add'});
    this.clearBtn = this.createElement('input', {'class': 'mtdl-clear-btn', 'type': 'button', 'value': 'Clear'});
    this.list = this.createElement('ul', {'class': 'mtdl-list'});

    this.inputs.appendChild(this.input);
    this.inputs.appendChild(this.createBtn);
    this.inputs.appendChild(this.clearBtn);
    this.title.appendChild(this.titleTxt);
    this.container.appendChild(this.title);
    this.container.appendChild(this.inputs);
    this.container.appendChild(this.list);
    todo.appendChild(this.container);

    this.createBtn.addEventListener('click', function(e) {
      if (self.input.value != '') {
        self.appendChild(self.input.value);
      }
    }, false);

    this.input.addEventListener('keypress', this.handleInputEnter.bind(this), false);
    this.clearBtn.addEventListener('click', this.handleClearClick.bind(this), false);
  }

  handleInputEnter(e) {
    if (e.keyCode == 13 && this.input.value != '') {
      this.addItem();
    }
  };

  addItem(text) {
    let self = this;
    let content = document.createTextNode(this.input.value);
    let del = this.createElement('span', {'class': 'mtdl-remove-item'});
    let item = this.createElement('li', {'class': 'mtdl-item'});

    del.innerHTML = '&#x2717';
    item.appendChild(content);
    item.appendChild(del);
    this.input.value = '';
    this.list.appendChild(item);

    item.addEventListener('click', function(e) {
      e.target.classList.toggle('mtdl-checked');
    });

    del.addEventListener('click', function(e) {
      self.list.removeChild(e.target.parentNode);
    });
  }

  handleClearClick(e) {
    let itemsChecked = this.container.querySelectorAll('.mtdl-checked');

  	for (let i = 0; i < itemsChecked.length; i++) {
  		itemsChecked[i].parentNode.removeChild(itemsChecked[i]);
  	}
  }
}
