import { Component } from './Component.js';
import '../styles/MyLightBox.css';

export class MyLightBox extends Component {
    constructor(images) {
        super();
        this.container = this.createElement("div", {"class": "mlb-container"});
        this.images = this.createElement("div", {"class": "mlb-images"});
        this.panelLeft= this.createElement("div", {"class": "mlb-pnl-left mlb-pnl"})
        this.panelRight= this.createElement("div", {"class": "mlb-pnl-right mlb-pnl"});
        this.buttonLeft = this.createElement("div", {"class": "mlb-btn-left mlb-btn"});
        this.buttonRight= this.createElement("div", {"class": "mlb-btn-right mlb-btn"});
        this.buttonDelete = this.createElement("div", {"class": "mlb-btn-delete"});
        this.left = this.createElement("span");
        this.right = this.createElement("span");
        this.delete = this.createElement("span");

        this.right.innerHTML = "&#10095";
        this.left.innerHTML = "&#10094";
        this.delete.innerHTML = '&#x2717';


        this.buttonDelete.appendChild(this.delete);
        this.buttonRight.appendChild(this.right);
        this.buttonLeft.appendChild(this.left);
        this.panelLeft.appendChild(this.buttonLeft);
        this.panelRight.appendChild(this.buttonRight);
        this.panelRight.appendChild(this.buttonDelete);
        this.container.appendChild(this.panelLeft);
        this.container.appendChild(this.panelRight);
        this.container.appendChild(this.images);

        for (let i = 0; i<images.length; i++) {
            let img = this.createElement("img", {"class": "mlb-image", "src": images[i]});
            this.images.appendChild(img);
        }

        this.images.childNodes[0].classList.add("mlb-image-active");

        this.buttonRight.addEventListener('click', this.next.bind(this), false);
        this.buttonLeft.addEventListener('click', this.previous.bind(this), false);
        this.buttonDelete.addEventListener('click', this.hide.bind(this), false);

        this.panelRight.addEventListener('mouseenter', this.showPanels.bind(this), false);
        this.panelLeft.addEventListener('mouseenter', this.showPanels.bind(this), false);
        this.panelRight.addEventListener('mouseleave', this.hidePanels.bind(this), false);
        this.panelLeft.addEventListener('mouseleave', this.hidePanels.bind(this), false);
    }

    show(index) {
        this.images.childNodes[index].classList.add("mlb-image-active");
    	document.body.appendChild(this.container);
    }

    hide(index) {
    	this.images.querySelector(".mlb-image-active").classList.remove("mlb-image-active");
    	document.body.removeChild(this.container);
    }

    next(e) {
    	let curr = this.images.querySelector(".mlb-image-active");
    	let next = curr.nextSibling ? curr.nextSibling : this.images.childNodes[0];

        curr.classList.remove("mlb-image-active");
        next.classList.add("mlb-image-active");
    }

    previous(e) {
    	let curr = this.images.querySelector(".mlb-image-active");
    	let prev = curr.previousSibling ? curr.previousSibling : this.images.childNodes[this.images.childNodes.length-1];

    	curr.classList.remove("mlb-image-active");
        prev.classList.add("mlb-image-active");
    }

    showPanels(e) {
        this.panelLeft.classList.add("mlb-pnl-active");
        this.panelRight.classList.add("mlb-pnl-active");
    }

    hidePanels(e) {
        this.panelLeft.classList.remove("mlb-pnl-active");
        this.panelRight.classList.remove("mlb-pnl-active");
    }
}
