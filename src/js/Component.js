export class Component {
	createElement(type, attributes={}) {
        let create = document.createElement(type);

        for (let attrName in attributes) {
            	create.setAttribute(attrName, attributes[attrName]);
        }


        return create;
	}
}
